cd
mkdir libretranslate
cd libretranslate
git clone https://git.fairkom.net/emb/displ.eu/transposer/service-connector
git clone https://git.fairkom.net/emb/displ.eu/transposer/shared-lib.git
cd shared-lib
cd modules
git clone https://git.fairkom.net/emb/displ.eu/transposer/modules/utils
cd ../adapters
git clone https://git.fairkom.net/emb/displ.eu/transposer/adapters/libretranslate.git
cd ../../service-connector
python3 -m venv ./venv
source venv/bin/activate
pip install -r requirements.txt
pip install -r ../shared-lib/modules/utils/requirements.txt
# pip install -r ../shared-lib/adapters/libretranslate/requirements.txt
