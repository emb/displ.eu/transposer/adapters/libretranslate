# general use imports
from requests.exceptions import HTTPError
import requests
from cerberus import Validator
import logging



# loads config etc.
from base_adapter import BaseAdapter


# payload params
INPUT =  "input"
SOURCE_LANGUAGE_ID = "source_language_id"
TARGET_LANGUAGE_ID = "target_language_id"
LIBRETRANSLATE_DOMAIN = "libretranslate_domain"
MARKUP = "markup"

CAPTION_ID = "caption_id"
UNIQUE_HASH = "uniqueHash"


PAYLOAD_SCHEMA = {
    INPUT: {'type': 'string', 'required': True},
    SOURCE_LANGUAGE_ID: {'type': 'string', 'required': True},
    TARGET_LANGUAGE_ID: {'type': 'string', 'required': True},
    MARKUP: {'type': 'string'}
}


class Adapter(BaseAdapter):
    def __init__(self, connector, config_path):
        super(Adapter, self).__init__(connector, config_path)
        print("LibreTranslate Adapter ... config:", self.config)
        self.libretranslate_domain = self.config.get("libretranslate").get(LIBRETRANSLATE_DOMAIN, "https://libretranslate.dev.displ.eu").rstrip("/")


    def run(self, payload, msg):
        result = {}
        success = False
        try:
            validator = Validator(PAYLOAD_SCHEMA)
            validator.allow_unknown = True
            if validator.validate(payload) is False:
                # validator.errors could be for example {'input': ['required field'], 'format': ['unallowed value ee']}
                logging.error(validator.errors)
                result['success'] = False
                return result

            source_language_id = payload[SOURCE_LANGUAGE_ID]
            target_language_id = payload[TARGET_LANGUAGE_ID]

            # optional params - should we create a whole new adapter for the captions and if yes should we make use of the text translate adapter to avoid duplicating code? probably yes
            # it's crucual to keep these before the actual translation which might raise an exception and skip the rest
            if CAPTION_ID in payload:
                result[CAPTION_ID] = payload[CAPTION_ID]
            index_param = "index"
            if index_param in payload:
                result[index_param] = payload[index_param]
            result["language_id"] = target_language_id
            if UNIQUE_HASH in payload:
                result[UNIQUE_HASH] = payload[UNIQUE_HASH]
            
            markup = None
            if MARKUP in payload:
                markup = payload[MARKUP]



            translated_text = self.translate(
                payload[INPUT],
                source_language_id,
                target_language_id,
                markup
            )

            result["translated_text"] = translated_text
            success = True

        except HTTPError as http_error:
            logging.exception("HTTPError Exception thrown")
            success = False
        except Exception as e:
            logging.exception("Exception thrown")
            success = False

        if "response_topic_id" in payload:
            result["topic"] = payload["response_topic_id"]
            self.connector.sendResponseTo(result)
            del result["topic"]

        result["success"] = success
        return result

    def translate(
        self, text, source_language_id, target_language_id, markup = None
    ):
        request_payload = {
            "q": text,
            "source": source_language_id,
            "target": target_language_id,
        }
        if markup is not None:
            request_payload["format"] = "html"

        # TODO change everywhere from contcatinating to os.path.join
        response = requests.post(
            f"{self.libretranslate_domain}/translate", data=request_payload
        )
        if response.status_code != 200:
            response.raise_for_status()
        return response.json()["translatedText"]


    def adapter_name(self):
        return 'libretranslate'